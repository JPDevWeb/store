@extends('template')

@section('conteudo')
<div class="container py-5">
    <h1> Editar Produto <a href="{{url('listar-produtos')}}" class="btn btn-outline-primary float-right">Voltar</a>
    </h1>
    <p class="text-success">{{session('success')}}</p>
    <p class="text-danger">{{session('error')}}</p>
    <form action="/atualizar-produto" method="POST">
        @csrf
        <input name="slug" type="hidden" value="{{$produto->slug}}">
        <input class="form-control my-2" type="text" name='nome' placeholder='Nome *'
            value="{{old('nome',$produto->nome)}}">
        <p class="text-danger my-2">{{$errors->first('nome')}}</p>
        <input class="form-control my-2" tyep='text' name='codigo_barra' placeholder="Código de barra *"
            value="{{old('codigo_barra',$produto->codigo_barra)}}">
        <p class="text-danger my-2">{{$errors->first('codigo_barra')}}</p>
        <textarea class="form-control my-2" name="descricao" rows="3"
            placeholder="Descrição *">{{old('descricao',$produto->descricao)}}</textarea>
        <p class="text-danger my-2">{{$errors->first('descricao')}}</p>
        <select class="form-control my-2" name="tipo">
            <option value=""> Escolha um tipo </option>
            <option value="1" @if(old('tipo',$produto->tipo) == 1) selected @endif> Eletromésticos </option>
            <option value="2" @if(old('tipo',$produto->tipo) == 2) selected @endif> Móveis </option>
            <option value="3" @if(old('tipo',$produto->tipo) == 3) selected @endif> Cama, mesa e banho </option>
        </select>
        <p class="text-danger my-2">{{$errors->first('tipo')}}</p>
        <input class="form-control my-2" type="text" name='preco' placeholder="Preço *"
            value="{{old('preco',$produto->preco)}}">
        <p class="text-danger my-2">{{$errors->first('preco')}}</p>
        <button type="submit"> Salvar </button>
    </form>
</div>
@endsection
