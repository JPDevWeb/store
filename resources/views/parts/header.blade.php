<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="font-size: 1.3em;">
    <a class="navbar-brand" href="{{url('/')}}">Seminário Computação</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{url('listar-produtos')}}">Produtos <span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{url('listar-tipos')}}">Tipos <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
