@extends('template')
@section('conteudo')
<div class="container py-5">
    <h1> Listar Tipos <a href="{{url('listar-produtos')}}" class="btn btn-outline-success float-right">Voltar</a></h1>
    <p class="text-success">{{session('success')}}</p>
    <p class="text-danger">{{session('error')}}</p>
    @foreach($tipos as $tipo)
    <h2>{{$tipo->nome}}</h2>
    @php
    $produtos = $tipo->produtos;
    @endphp
    @foreach($produtos as $produto)
    <p class="text-primary">{{$produto->nome}} do tipo <span class="text-success">{{$tipo->nome}}</span></p>
    @endforeach
    @endforeach

</div>
@endsection
