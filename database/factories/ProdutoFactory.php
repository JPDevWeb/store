<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Produto;

$factory->define(Produto::class, function (Faker $faker) {
    return [
        'slug' => time() . rand(1111, 9999),
        'nome' => $faker->name,
        'descricao' => $faker->text(500),
        'preco' => rand(1, 3000),
        'tipo_id' => rand(1, 5),
        'codigo_barra' => $faker->text(10)
    ];
});
