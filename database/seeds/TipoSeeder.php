<?php

use Illuminate\Database\Seeder;
use App\Models\Tipo;

class TipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tipo::create(['nome' => 'Eletroméstico']);
        Tipo::create(['nome' => 'Móveis']);
        Tipo::create(['nome' => 'Informártica']);
        Tipo::create(['nome' => 'Cozinha']);
        Tipo::create(['nome' => 'Mesa e banho']);
    }
}
